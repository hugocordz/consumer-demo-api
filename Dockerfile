FROM openjdk:11-slim

WORKDIR /vol/demo-api

COPY ./target/*.jar app.jar

CMD ["java", "-jar", "app.jar"]


