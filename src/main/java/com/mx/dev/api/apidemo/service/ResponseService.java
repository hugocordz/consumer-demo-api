package com.mx.dev.api.apidemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class ResponseService {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    public void sendResponse(Message message) {
        try {
            log.info("Starting sending back");
            String messageStr = "Hello there " + UUID.randomUUID().toString();
            Message responseMessage = new Message(messageStr.getBytes(), message.getMessageProperties());
            rabbitTemplate.send(message.getMessageProperties().getReplyTo(), responseMessage);
        } catch (Exception e) {
            log.error("error sending message", e);
        }
    }
}
