package com.mx.dev.api.apidemo.consumer;

import com.mx.dev.api.apidemo.service.ResponseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class QueueConsumer {

    @Autowired
    private ResponseService responseService;

    @RabbitListener(queues = "rest.api.example")
    public void processMessage(Message message) {
        String body = new String(message.getBody());
        log.info("Message Received: {}", body);
        responseService.sendResponse(message);
    }
}
